# iCelera - iBlue52 Python Integration

iCelera iBlue52 communication software and documentation.

## How to use

    $ python3 icelera.py -h

## Requirements

    Python3.5+
    Pandas

# Reference

If you want to use this material please cite this reference:

Bechelli, R. P. Software iCelera - iBlue52 Python Integration, on-line, São Paulo, v0.2.0, 2016. Disponível: https://bitbucket.org/rodrigoprior/icelera_iblue52_integration/ . Acesso em 12 dez 2016.

    @misc{bechelliicelera2016,
        address = {São Paulo},
        author = {Bechelli, R. P.},
        title = {Software iCelera - iBlue52 Python Integration (0.2.0)},
        year = {2016},
        url = {https://bitbucket.org/rodrigoprior/icelera_iblue52_integration/},
    }

# Additional information

## Financiamento

### Software

O desenvolvimento do software de integração foi financiado através de bolsa de estudo de doutorado do aluno Rodrigo Prior Bechelli.

Orientador: Profa. Dra. Maria Claudia Ferrari de Castro

### Hardware

O Equipamento foi comprado através do Edital CAPES Pró-Equipamentos IES Comunitárias (051/2012)

Subprojeto 1: “EEG em Interface Cérebro-Computador”

Coordenador: Prof. Dr. Plinio Thomaz Aquino Junior
