# Rotina de Leitura de Dados de Rede - iCelera/iBlue52 #

Documentos em anexo: docs/RotinadeleituradoiBlue.docx

Anexo a rotina de leitura dos dados recebidos da rede. Está em Visual Basic 6.0. Tudo que começa com apóstrofe é comentário.
Acho que o ponto mais importante que conversamos sobre a leitura está nas primeiras e nas últimas linhas da rotina. Não me importo com quantos dados chegaram. Se a quantidade de dados a serem analisados é menor que o bloco mínino, deixo esses dados para serem concatenados com os próximos que chegarem.

Primeiras:

    If UBound(g_dadossobraram) > 0 Then
        ReDim dados_aux(UBound(dados)) As Byte
        CopyMemory dados_aux(0), dados(0), UBound(dados_aux) + 1
        
        ReDim dados(UBound(g_dadossobraram) + UBound(dados_aux)) As Byte
        CopyMemory dados(0), g_dadossobraram(0), UBound(g_dadossobraram)
        CopyMemory dados(UBound(g_dadossobraram)), dados_aux(0), UBound(dados_aux) + 1
        ReDim g_dadossobraram(0) As Byte
    End If

Últimas:

    If Pos <= UBound(dados) Then
        ReDim g_dadossobraram(UBound(dados) - Pos + 1) As Byte
        CopyMemory g_dadossobraram(0), dados(Pos), UBound(g_dadossobraram)
    End If
