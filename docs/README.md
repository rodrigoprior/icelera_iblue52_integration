# Additional details

## Telnet

A iCelera usa telnet para configurar o equipamento

Eles utilizam o hyperterminal do windows com os settings:

    VT100
    VT52
    ASCII SETUP
    ip: 192.168.5.10
    port: 9000

### Exemplo de Comando:

#### Alterar a data dos dispositivo

Este comando altera a data do dispositivo para

    $ DATA 26/05/16

## Como os dados são enviados

Os dados são enviados como **Byte** na comunicação com TCP/IP.

A galera da iCelera usa no software deles o componente **Winsock** da Microsoft.

A documentação se encontra no link:

https://msdn.microsoft.com/en-us/library/aa228119(v=vs.60).aspx


## Lista de palavras iCelera

| sinal                 | palavra hex |
| --------------------- | ----------- |
| nao sei               | hex(0x77ee7400) |
| gravacao no cartao    | hex(0x77ee473700) |
| inicio comunicacao    | hex(0x77ee55) |
| lista de eletrodos    | hex(0x77ee550077ee43010101010101010101010101010101010101010101010101) |
| lista de eletrodos    | hex(0x77ee43010101010101010101010101010101010101010101010101) |


## Ascii Table

| ascii | hex | dec |
| ----- | --- | --- |
|   C   |  43 |  -  |
|   G   |  47 |  -  |
|   U   |  55 |  -  |
|   t   |  74 |  -  |

Extraido de http://ascii-code.com/

## Monitorar interface de rede

    $ sudo tcpdump -n -i lo -A -XX -x dst port 9999 and greater 70

## References

- https://docs.python.org/3.5/library/socket.html
- https://docs.python.org/3.5/library/socketserver.html
- https://docs.python.org/3.5/howto/sockets.html
- http://www.binarytides.com/receive-full-data-with-the-recv-socket-function-in-python/
- http://www.informit.com/articles/article.aspx?p=2234249
- http://www.ibm.com/developerworks/library/l-python-state/index.html
- http://bt3gl.github.io/black-hat-python-networking-the-socket-module.html
