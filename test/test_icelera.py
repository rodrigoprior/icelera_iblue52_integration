"""
To run unittests:
    $ python -m unittest discover test
"""
import unittest
import struct
import icelera
import numpy as np

ic = icelera.iblue()
ic.host = '192.168.5.10'
ic.port = 9000
ic.group = ['eeg']

class ValuesTest(unittest.TestCase):
    def test(self):
        self.assertEqual(ic.host, '192.168.5.10')
        self.assertEqual(ic.port, 9000)
        self.assertEqual(ic.group, ['eeg'])
        self.assertEqual(ic.channels, [])

    def test_channel_selector(self):
        self.assertEqual(
            ic.channel_selector(group=['eeg']),
            bytearray(b'w\xeeC\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x00\x00\x01\x00\x00\x01\x00\x00\x01\x00\x00\x01\x00\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x00\x00\x01\x00\x00\x01\x00\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
            )
        self.assertEqual(
            ic.channel_selector(group=['emg']),
            bytearray(b'w\xeeC\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
        )
        self.assertEqual(
            ic.channel_selector(group=['eeg', 'emg']),
            bytearray(b'w\xeeC\x01\x01\x01\x01\x01\x00\x01\x01\x00\x01\x00\x00\x01\x00\x00\x01\x00\x01\x01\x00\x00\x01\x01\x00\x01\x01\x01\x01\x01\x00\x01\x01\x00\x01\x00\x00\x01\x00\x01\x01\x00\x00\x01\x00\x00\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
        )
        self.assertEqual(
            ic.channel_selector(group=['eeg', 'eeg+']),
            bytearray(b'w\xeeC\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x00\x00\x01\x00\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
        )
        self.assertEqual(
            ic.channel_selector(group=['eeg', 'eeg+', 'emg']),
            bytearray(b'w\xeeC\x01\x01\x01\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x01\x01\x00\x00\x01\x01\x00\x01\x01\x01\x01\x01\x00\x01\x01\x00\x01\x01\x00\x01\x01\x01\x01\x01\x00\x01\x00\x00\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
        )

    def test_channel_names(self):
        self.assertEqual(
            ic.channel_names(struct.unpack('48c', ic.channel_selector(['eeg'])[3:-13])),
            ['A1', 'Pz', 'Fp1', 'T6', 'F7', 'O2', 'Fz', 'F8', 'C3', 'C4', 'T5', 'A2', 'P4', 'Fp2', 'O1', 'F3', 'Oz', 'F4', 'T3', 'Cz', 'T4', 'P3']
        )
        self.assertEqual(
            ic.channel_names(struct.unpack('48c', ic.channel_selector(['emg'])[3:-13])),
            ['EX3', 'EX6', 'EX1', 'EX4', 'EX5', 'EX2']
        )
        self.assertEqual(
            ic.channel_names(struct.unpack('48c', ic.channel_selector(['eeg', 'emg'])[3:-13])),
            ['A1', 'Pz', 'EX3', 'Fp1', 'T6', 'F7', 'O2', 'Fz', 'F8', 'C3', 'EX6', 'C4', 'T5', 'EX1', 'A2', 'P4', 'EX4', 'Fp2', 'O1', 'F3', 'Oz', 'F4', 'T3', 'EX5', 'Cz', 'T4', 'P3', 'EX2']
        )
        self.assertEqual(
            ic.channel_names(struct.unpack('48c', ic.channel_selector(['eeg', 'eeg+'])[3:-13])),
            ['A1', 'Pz', 'Fp1', 'T6', 'F7', 'O2', 'Fz', 'E3', 'F8', 'E1', 'C3', 'E5', 'C4', 'T5', 'A2', 'P4', 'Fp2', 'O1', 'F3', 'Oz', 'F4', 'E4', 'T3', 'E2', 'Cz', 'E6', 'T4', 'P3']
        )
        self.assertEqual(
            ic.channel_names(struct.unpack('48c', ic.channel_selector(['eeg', 'eeg+', 'emg'])[3:-13])),
            ['A1', 'Pz', 'EX3', 'Fp1', 'T6', 'F7', 'O2', 'Fz', 'E3', 'F8', 'E1', 'C3', 'E5', 'EX6', 'C4', 'T5', 'EX1', 'A2', 'P4', 'EX4', 'Fp2', 'O1', 'F3', 'Oz', 'F4', 'E4', 'T3', 'E2', 'EX5', 'Cz', 'E6', 'T4', 'P3', 'EX2']
        )

    def test_channel_convrate(self):
        np.testing.assert_array_equal(
            ic.channel_convrate(group=['eeg']),
            np.array([25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72])
            )
        np.testing.assert_array_equal(
            ic.channel_convrate(group=['emg']),
            np.array([5149257.143, 5149257.143, 5149257.143, 5149257.143, 5149257.143, 5149257.143])
            )
        np.testing.assert_array_equal(
            ic.channel_convrate(group=['eeg', 'emg']),
            np.array([25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 25746285.72, 5149257.143])
            )
        np.testing.assert_array_equal(
            ic.channel_convrate(group=['eeg', 'eeg+']),
            np.array([25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72])
            )
        np.testing.assert_array_equal(
            ic.channel_convrate(group=['eeg', 'eeg+', 'emg']),
            np.array([25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 5149257.143, 25746285.72, 25746285.72, 25746285.72, 25746285.72, 5149257.143])
            )
